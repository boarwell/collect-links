import { extractURL } from './html/index.js';
import { parseArgs, print } from './interface/index.js';
import { request } from './request/index.js';
import {
  buildURLUpdater,
  addProtcol,
  buildHostFilter,
  isHTML
} from './url/index.js';

const main = async () => {
  const inputURL = parseArgs();

  const updateURL = buildURLUpdater(inputURL);
  const isInSameHost = buildHostFilter(inputURL);

  const found = new Set<string>();
  const visited = new Set<string>();
  let queue = [inputURL.href];

  while (queue.length > 0) {
    const currentURLString = queue.shift();
    if (currentURLString === undefined) {
      break;
    }
    visited.add(currentURLString);

    // キューに入れる時点で不正なものを弾いているのでここではキャストする
    const current = updateURL(currentURLString) as URL;

    const body = await request(current);
    const urls = (extractURL(body)
      .map(addProtcol)
      .map(updateURL)
      .filter(url => url !== null) as URL[])
      .filter(isInSameHost)
      .filter(isHTML);

    urls.forEach(url => found.add(url.pathname));

    const notVisitedURLs = urls
      .map(url => url.pathname)
      .filter(url => !visited.has(url));

    queue = [...new Set([...queue, ...notVisitedURLs])];
  }

  print(found);
};

main();
