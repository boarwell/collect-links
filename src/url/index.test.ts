import * as u from './index.js';

test('updateURL', () => {
  expect(u.updateURL('#test')).toBe(null);
  expect(u.updateURL('?v=xyz')).toBe(null);
});

test('isHTML', () => {
  // 表記違い
  expect(u.isHTML(new URL('https://example.com'))).toBe(true);
  expect(u.isHTML(new URL('https://example.com/'))).toBe(true);
  expect(u.isHTML(new URL('https://example.com/index.html'))).toBe(true);
  expect(u.isHTML(new URL('https://example.com/#test'))).toBe(true);
  expect(u.isHTML(new URL('https://example.com/?v=xyz'))).toBe(true);
  // 拡張子違い
  expect(u.isHTML(new URL('https://example.com/test.pdf'))).toBe(false);
  expect(u.isHTML(new URL('https://example.com/test.css'))).toBe(false);
  // プロトコル違い
  expect(u.isHTML(new URL('mailto://test@example.com'))).toBe(false);
});

test('buildHostFilter', () => {
  const f = u.buildHostFilter(new URL('https://example.com'));
  const cases = [
    {
      url: 'https://example.com/test/',
      expected: true
    },
    {
      url: 'http://example.com/test/',
      expected: true
    },
    {
      url: 'https://example.com:8080/test/',
      expected: false
    },
    {
      url: 'https://example.org/test/',
      expected: false
    },
    {
      url: 'ftp://example.com/test/',
      expected: true
    }
  ];

  cases.forEach(testcase => {
    expect(f(new URL(testcase.url))).toBe(testcase.expected);
  });
});
