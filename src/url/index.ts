// 絶対URLを文字列で受け取ってURLオブジェクトを返す
// 不正なURLのときはnullを返す
export const updateURL = (urlString: string): URL | null => {
  try {
    const url = new URL(urlString);
    return url;
  } catch (err) {
    console.error(err);
    return null;
  }
};

export const buildURLUpdater = (baseURL: URL) => (url: string): URL | null => {
  try {
    const updated = new URL(url, baseURL);
    return updated;
  } catch (e) {
    return null;
  }
};

export const addProtcol = (s: string) => {
  return s.startsWith('//') ? `https:${s}` : s;
};

export const isHTML = (url: URL): boolean => {
  const { pathname, protocol } = url;

  const withHTMLExtension = /\.html?$/.test(pathname);
  const isPlainFile = !pathname.includes('.');
  const isDirectory = pathname.endsWith('/');
  const isHTTP = /https?/.test(protocol);

  return isHTTP && (withHTMLExtension || isPlainFile || isDirectory);
};

/**
 * 自サイト内のリンクのみを抽出するための関数を作る
 * 最初に自サイトのURLを渡す
 */
export const buildHostFilter = (canonical: URL) => (url: URL): boolean => {
  return canonical.host === url.host;
};
