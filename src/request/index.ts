import got from 'got';

/**
 * urlを受け取りGetリクエストを投げてレスポンスボディを返す
 * ライブラリの差し替えを簡単にしておくために抽象化しておく
 */
export const request = async (url: URL): Promise<string> => {
  // 日本語のURLをエンコードしてくれる
  try {
    const response = await got(url.href);

    return response.body;
  } catch (err) {
    console.error(err);

    return '';
  }
};
