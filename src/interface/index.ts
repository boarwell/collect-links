/**
 * 結果の出力
 */
export const print = <T>(set: Set<T>): void => {
  console.log([...set].join('\n'));
};

export const parseArgs = () => {
  const target = process.argv[2];
  // const target = 'https://mailwise.cybozu.co.jp/';
  if (target === undefined) {
    console.log('specify target url');
    return process.exit(1);
  }
  try {
    const url = new URL(target);
    return url;
  } catch (error) {
    console.error(error);
    return process.exit(1);
  }
};
